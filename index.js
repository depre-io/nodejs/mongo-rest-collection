(function () {
        'use strict'
        const crypto = require('crypto')
        const _ = require('lodash')
        const deepDash = require('deepdash')
        const jsonpatch = require('fast-json-patch')
        const monk = require('monk')
        const assign = require('object-assign')
        const safeSet = require('just-safe-set')
        const safeGet = require('just-safe-get')
        const httpResponse = require('http-response')

        deepDash(_)

        const defaultPermissions = {
            C: false,
            R: false,
            U: false,
            D: false,
            L: false
        }

        const defaultValidator = require('./validator.json')

        function findMappings(data) {
            let result = []
            const searchKey = '_map'
            _.eachDeep(data, (map, key, path, depth, parent, parentKey, parentPath) => {
                if (key === searchKey) {
                    result.push({map, path, parent, parentKey, parentPath})
                    return false // return false explicitly to skip iteration over current value's children
                }
            })
            return result
        }

        module.exports.Collection = class Collection {

            constructor(collection, options) {
                this.collection = collection
                this.permissions = assign(defaultPermissions, options.permissions)
                this.validator = assign(defaultValidator, options.validator)
            }

            registerRoutes(router, route) {
                this.route = route
                router.get('/_meta/permissions', this.getPermissions.bind(this))
                router.get('/_meta/validator', this.getValidator.bind(this))
                this.permissions.C && router.post('/:id?/:sha256?', this.post.bind(this))
                this.permissions.C && router.patch('/:id?/:sha256?', this.post.bind(this))
                this.permissions.R && router.get('/:id/:sha256?', this.readById.bind(this))
                this.permissions.D && router.delete('/:id/:sha256?', this.deleteById.bind(this))
                this.permissions.L && router.get('/', this.read.bind(this))
            }

            checkPermissions(req, res, permission) {
                if (!this.readPermissions(req, res, permission)) {
                    httpResponse.forbidden(res)(this.permissions[permission])
                    return false
                }
                return true
            }

            readPermissions(req, res, permission) {
                const p = this.permissions[permission]
                if (typeof p === "boolean") {
                    return p
                } else {
                    return !!(typeof p !== 'undefined' && p.length && _.intersection(p, req.user.roles).length)
                }


            }

            parseQueryParameter(req, res, field) {
                const str = req.query[field]
                let result
                if (str) {
                    try {
                        result = {}
                        result[field] = JSON.parse(decodeURIComponent(str))
                    } catch (e) {
                        httpResponse.badRequest(res)('Syntax error in query paramater ' + field)
                    }
                }
                return result
            }

            getFields(req, res) {
                return this.parseQueryParameter(req, res, 'fields')
            }

            getFilter(req, res) {
                return this.parseQueryParameter(req, res, 'filter')
            }

            getSort(req, res) {
                return this.parseQueryParameter(req, res, 'sort')
            }

            getSkip(req, res) {
                return this.parseQueryParameter(req, res, 'skip')
            }

            getLimit(req, res) {
                return this.parseQueryParameter(req, res, 'limit')
            }

            getPermissions(req, res) {
                const result = {
                    C: this.readPermissions(req, res, 'C'),
                    R: this.readPermissions(req, res, 'R'),
                    U: this.readPermissions(req, res, 'U'),
                    D: this.readPermissions(req, res, 'D'),
                    L: this.readPermissions(req, res, 'L')
                }
                httpResponse.ok(res)(result)
            }

            getValidator(req, res) {
                httpResponse.ok(res)(this.validator)
            }

            async create(req, res) {
                try {
                    this.checkPermissions(req, res, 'L')

                    const {filter} = this.getFilter(req, res) || {}
                    const fields = this.getFields(req, res) || {}
                    const sort = this.getSort(req, res)
                    const skip = this.getSkip(req, res)
                    const limit = this.getLimit(req, res)
                    const options = {...fields, ...skip, ...limit, ...sort}

                    const response = await req.db.get(this.collection, {castIds: false})
                        .find(filter, options)
                    const result = await this.mapResponseCollection(req, res, response)
                    httpResponse.ok(res)(result)
                    return result
                } catch (e) {
                    httpResponse.badRequest(res)(e)
                }
            }

            getValidator(req, res) {
                httpResponse.ok(res)(this.validator)
            }


            async read(req, res) {
                if (!this.checkPermissions(req, res, 'L')) return
                const objectIdRegex = /ObjectId\(([0-9a-fA-F]{24})\)/g;
                try {
                    let filter = (this.getFilter(req, res) || {}).filter
                    filter = filter && _.mapValuesDeep(
                        filter,
                        v => (typeof v === "string" && objectIdRegex.test(v))
                            ? toObjectId(v.replace(/ObjectId\(([0-9a-fA-F]{24})\)/g, (_matches, id)=> id ))
                            : v,
                        { leavesOnly: true }
                    );
                    let  fields = this.getFields(req, res) || {}
                    const sort = this.getSort(req, res)
                    const skip = this.getSkip(req, res)
                    const limit = this.getLimit(req, res)
                    const options = {...fields, ...skip, ...limit, ...sort}

                    const response = await req.db.get(this.collection, {castIds: false})
                        .find(filter, options)
                    const result = await this.mapResponseCollection(req, res, response)
                    httpResponse.ok(res)(result)
                    return result
                } catch (e) {
                    httpResponse.badRequest(res)(e)
                }
            }


            async readById(req, res, xId) {
                if (!this.checkPermissions(req, res, 'R')) return
                try {
                    const subCall = !_.isFunction(xId)
                    const filter = subCall ? {_id: toObjectId(xId)} : {_id: toObjectId(req.params.id)}
                    const options = this.getFields(req, res)
                    const response = await req.db.get(this.collection, {castIds: false})
                        .findOne(filter, options)
                    if (response) {
                        const result = await this.mapResponseCollection(req, res, response)
                        subCall || httpResponse.ok(res)(result)
                        return result
                    } else {
                        httpResponse.notFound(res)()
                    }
                } catch (e) {
                    httpResponse.badRequest(res)(e)
                }

            }

            async remove(req, res, xId, data) {
                if (!this.checkPermissions(req, res, 'D')) return
                try {
                    const subCall = !_.isFunction(xId)
                    data = data || this.from_frontend(req.body)
                    let filter = subCall ? {_id: toObjectId(xId)} : {id: toObjectId(req.params.id || data.id)}
                    let collection = req.db.get(this.collection, {castIds: false})

                    if (subcall || req.params.id || data.id) {
                        const {_locked} = await this.readById(req, res, id)
                        if (_locked) {
                            httpResponse.forbidden(res)('Document is locked for deletion')
                        }
                        await collection.update({_id: id}, {$unset: data})
                        const result = await this.readById(req, res, id)
                        subCall || httpResponse.ok(res)(result)
                        return result

                    } else {
                        httpResponse
                            .notFound(res)()
                    }
                } catch (e) {
                    httpResponse.badRequest(res)(e)
                }
            }

            async post(req, res, xData) {
                try {
                    const subCall = !_.isFunction(xData)
                    const collection = req.db.get(this.collection, {castIds: false})
                    if (!subCall) {
                        const id = req.params.id
                        !id && httpResponse.badRequest(res)("Missing id")
                        if (!this.checkPermissions(req, res, 'U')) return

                        let filter = toObjectId(id)
                        const original = await req.db.get(this.collection, {castIds: false})
                            .findOne(filter)
                        if (!original) {
                            return httpResponse.notFound(res)()
                        }
                        if (original._locked) {
                            return httpResponse.forbidden(res)('Document is locked for changes')
                        }
                        const data = await this.mapRequest(req, res, original, req.body )

                        const changed = this.addRevision(req, res, data, original)
                        if (!changed) {
                            return httpResponse.ok(res)(original)
                        }
                        await collection.update({_id: toObjectId(id)}, {$set: data})
                        const mappedData = await this.mapResponseCollection(req, res, data)
                        httpResponse.ok(res)(mappedData)
                        return data

                    } else {
                        if (xData._id){
                            this.addRevision(req, res, xData, xData)
                            await collection.update({_id: xData._id}, {$set: xData})
                            return xData
                        } else {
                            this.addRevision(req, res, xData, xData, true)
                            const result = await collection.insert(xData, {upsert: true})
                            return result
                        }
                    }
                } catch (e) {
                    httpResponse.badRequest(res)(e)
                }
            }

            async deleteById(req, res, xId) {
                if (!this.checkPermissions(req, res, 'D')) return

                const subCall = !_.isFunction(xId)
                const id = subCall ? xId : req.params.id
                id || httpResponse.badRequest(res)('No id specified')

                if (_.startsWith(id, "_")) {
                    return httpResponse.forbidden(res)('Templates cannot be deleted')
                }

                const {_locked} = await this.readById(req, res, id)
                if (_locked) {
                    httpResponse.forbidden(res)('Document is locked for deletion')
                }
                let filter = subCall ? {_id: toObjectId(xId)} : {_id: toObjectId(req.params.id)}
                try {
                    const response = await req.db.get(this.collection, {castIds: false}).remove(filter)
                    if (response) {
                        subCall || httpResponse.ok(res)(response)
                        return response
                    } else {
                        httpResponse.badRequest(res)()
                    }
                } catch (e) {
                    httpResponse.badRequest(res)
                }
            }

            addRevision(req, res, newVersion, oldVersion, force) {
                let _revisions = oldVersion._revisions
                delete (oldVersion._revisions)
                delete (newVersion._revisions)
                const oldData = _.pickBy(oldVersion, (value, key) => {
                    return !_.startsWith(key, "_")
                })
                oldData._sha256 = oldData._sha256 || crypto.createHash('sha256').update(JSON.stringify(oldData)).digest('hex')

                const newData = _.pickBy(newVersion, (value, key) => {
                    return !_.startsWith(key, "_")
                })
                const _sha256 = crypto.createHash('sha256').update(JSON.stringify(newData)).digest('hex')
                newData._sha256 = _sha256
                newVersion._sha256 = newData._sha256

                var patches = jsonpatch.compare(newData, oldData) //store the previous value
                //cleanout diff from internal var changes
                patches = _.reject(patches, (obj) => {
                    return obj.path.indexOf('/_') === 0
                })
                if (!!patches.length || force) {
                    var row = {
                        user: {id: req.user.id, name: req.user.name, href: req.user.href},
                        date: new Date(),
                        sha256: oldData._sha256,
                        patches
                    }

                    if (!_.isArray(_revisions)) {
                        _revisions = []
                    }
                    _revisions.push(row)
                }

                newVersion._meta = newVersion._meta || {}
                newVersion._meta[this.collection] = newVersion._meta[this.collection] || {}
                newVersion._meta[this.collection].created = newVersion._meta[this.collection].created || {
                    user: {id: req.user.id, name: req.user.name, href: req.user.href},
                    date: new Date()
                }
                if(patches.length){
                    newVersion._meta[this.collection].modified = {
                        user: {id: req.user.id, name: req.user.name, href: req.user.href},
                        date: new Date()
                    }
                }
                newVersion._revisions = _revisions
                newVersion._revision = _revisions.length

                return !!patches.length
            }


            async mapRequest(req, res, original, data) {
                data._id = toObjectId(data.id)
                delete data.id
                return data
            }

            async mapResponse(req, res, data, original) {
                data.id = fromObjectId(data._id)
                delete data._id
                delete data.href
                data._href = this.route + '/' + data.id + '/' + data._sha256
                return data
            }

            async mapResponseCollection(req, res, data) {
                if (Array.isArray(data)) {
                    for (const subData of data) {
                        await this.mapResponse(req, res, subData)
                    }
                } else if (typeof data === 'object') {
                    data = await this.mapResponse(req, res, data)
                }
                return data
            }
        }

        const fromObjectId = (id) => {
            try {
                if (!_.isObject(id) && (/^[0-9a-fA-F]{24}$/.test(id))) {
                    return new monk.id(id)
                } else {
                    return id
                }
            } catch (e) {
                return id
            }
        }
        module.exports.frombjectId = fromObjectId

        const toObjectId = (id) => {
            try {
                if (/^[0-9a-fA-F]{24}$/.test(id)) {
                    return new monk.id(id)
                } else {
                    return id
                }
            } catch (e) {
                return id
            }
        }
        module.exports.toObjectId = toObjectId
    }()
)

